## INTRODUCTION

The ckeditor5-chatGPT module provides some buttons in CKeditor 5 which will improve the spelling of a text

## REQUIREMENTS

This module requires the following modules:

- [ckeditor5](https://www.drupal.org/project/ckeditor5)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Configure this module at (/admin/config/content/formats/ckeditor5-chatgpt).

## MAINTAINERS

Current maintainers for Drupal 10:

- Laurent Doreille (ldo_dev) - https://www.drupal.org/u/ldo_dev
- Revathi B (Revathi.B) - https://www.drupal.org/u/revathib

