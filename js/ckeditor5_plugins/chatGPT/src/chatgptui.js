/**
 * @file registers the simpleBox toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core'
import { ButtonView } from 'ckeditor5/src/ui'
import icon from '../../../../icons/ckeditor5_chatgpt.svg'

export default class ChatGPTUI extends Plugin {
  init () {
    const editor = this.editor

    editor.ui.componentFactory.add('chatGPTButton', (locale) => {
      const command = editor.commands.get('insertchatGPT')
      const buttonView = new ButtonView(locale)

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('ChatGPT'),
        icon,
        tooltip: true
      })

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled')

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('insertchatGPT')
      )

      return buttonView
    })
  }
}
