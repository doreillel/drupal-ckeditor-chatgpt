import ChatGPTEditing from './chatgptediting'
import ChatGPTUI from './chatgptui'
import { Plugin } from 'ckeditor5/src/core'

export default class ChatGPTButton extends Plugin {
  static get requires () {
    return [ChatGPTEditing, ChatGPTUI]
  }
}
